<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{

    // 将隐性路由中id字段改为slug
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function creator()
    {
        return $this->belongsTo(User::class,'user_id'); // 使用 user_id 字段进行模型关联
    }

//    public function channel()
//    {
//        return $this->belongsTo(Channel::class);
//    }

    public function threads()
    {
        return $this->hasMany(Thread::class);
    }
}
