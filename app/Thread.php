<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Events\ThreadReceivedNewReply;
use Illuminate\Support\Facades\Redis;
use App\Events\ThreadHasNewReply;

class Thread extends Model
{
    use RecordsActivity,RecordsVisits;

    protected $guarded = [];
    protected $with = ['creator', 'channel'];
    protected $appends = ['isSubscribedTo'];
    protected $casts = [
        'locked' => 'boolean' // 加入这个参数前端报错
    ];

    protected static function boot()
    {
        parent::boot();

//        static::addGlobalScope('replyCount',function ($builder){
//            $builder->withCount('replies');
//        });

        static::deleting(function ($thread) {
            // $thread->replies()->delete();
            // 我们把批量删除与话题关联的回复改为逐条删除，使得Reply模型的deleting事件被监听到，
            // 从而删除与该回复相关的动作流
            $thread->replies->each->delete();
        });

    }

    public function path()
    {
        return "/threads/{$this->channel->slug}/{$this->id}";
    }


    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }


    public function creator()
    {
        return $this->belongsTo(User::class,'user_id'); // 使用 user_id 字段进行模型关联
    }


    public function replies()
    {
        return $this->hasMany(Reply::class)
            ->withCount('favorites')
            ->with('owner');
    }

    public function channel()
    {
        return $this->belongsTo(Channel::class,'channel_id');
    }


    public function subscriptions()
    {
        return $this->hasMany(ThreadSubscription::class);
    }


    public function getIsSubscribedToAttribute()
    {
        return $this->subscriptions()
            ->where('user_id',auth()->id())
            ->exists();
    }





    public function addReply($reply)
    {
        $reply = $this->replies()->create($reply);

        // Prepare notifications for all subscribers
//        $this->subscriptions
// //           ->filter (function ($sub) use ($reply){
// //               return $sub->user_id != $reply->user_id;
// //           })
//            ->where('user_id','!=',$reply->user_id)
//            ->each
//            ->notify($reply);

//        event(new ThreadHasNewReply($this,$reply));

//        $this->notifySubscribers($reply);

        event(new ThreadReceivedNewReply($reply));

        return $reply;
    }

//    public function notifySubscribers($reply)
//    {
//        $this->subscriptions
//            ->where('user_id','!=',$reply->user_id)
//            ->each
//            ->notify($reply);
//    }

    public function subscribe($userId = null)
    {
        $this->subscriptions()->create([
            'user_id' => $userId ?: auth()->id()
        ]);

        return $this;
    }

    public function unsubscribe($userId = null)
    {
        $this->subscriptions()
            ->where('user_id',$userId ?: auth()->id())
            ->delete();
    }

    public function hasUpdatesFor($user)
    {
        // Look in the cache for the proper key
        // compare that carbon instance with the $thread->updated_at

        $key = $user->visitedThreadCacheKey($this);

        return $this->updated_at > cache($key);
    }

    public function markBestReply(Reply $reply)
    {
        $this->update(['best_reply_id' => $reply->id]);
    }

}
