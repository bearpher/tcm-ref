<?php

namespace App\Policies;

use App\User;
use App\Reply;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReplyPolicy
{
    use HandlesAuthorization;


    public function create(User $user)
    {
        // 当作为属性访问 Eloquent 关联时，关联数据是「懒加载」的。
        // 这意味着在你第一次访问该属性时，才会加载关联数据
        if(! $lastReply = $user->fresh()->lastReply) {
            return true;
        }

        return ! $lastReply->wasJustPublished();
    }

    public function update(User $user, Reply $reply)
    {
        return $reply->user_id == $user->id;
    }
}
