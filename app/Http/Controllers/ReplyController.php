<?php

namespace App\Http\Controllers;

use App\User;
use App\Reply;
use App\Thread;
use App\Inspections\Spam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Notifications\YouWereMentioned;
use App\Http\Requests\CreatePostRequest;


class ReplyController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($channelId,Thread $thread)
    {
        return $thread->replies()->paginate(20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /*
    public function store($channelId, Thread $thread)
    {

        if(Gate::denies('create',new Reply)) {
            return response(
                'You are posting too frequently.Please take a break.:)',422
            );
        };

        try{
//            $this->authorize('create',new Reply);

            $this->validate(request(),['content' => 'required|spamfree']);

            $reply = $thread->addReply([
                'content' => request('content'),
                'user_id' => auth()->id(),
            ]);
        }catch (\Exception $e){
            return response(
                'Sorry,your reply could not be saved at this time.' ,422
            );
        }

        if(request()->expectsJson()){
            return $reply->load('owner'); // 我们预加载了owner：$reply->load('owner')
        }

        return back()->with('flash','Your reply has been left.');
    }
    */
    /**
     * @param $channelId
     * @param Thread $thread
     * @param CreatePostRequest $request
     * @return $this
     */
    public function store($channelId, Thread $thread, CreatePostRequest $form)
    {

        if($thread->locked) {
            return response('Thread is locked.',422);
        }

        return $thread->addReply([
            'content' => request('content'),
            'user_id' => auth()->id(),
        ])->load('owner');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function show(Reply $reply)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function edit(Reply $reply)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function update(Reply $reply)
    {
        $this->authorize('update',$reply);

        request()->validate(['content' => 'required|spamfree']);

        $reply->update(request(['content']));

    }

//    protected function validateReply()
//    {
//        $this->validate(request(),['content' => 'required']);
//
//        resolve(Spam::class)->detect(request('content'));
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reply $reply)
    {
        $this->authorize('update',$reply);

        $reply->delete();

        if (request()->expectsJson()){
            return response((['status' => 'Reply deleted']));
        }

        return back();
    }
}
