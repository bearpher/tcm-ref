<?php

namespace App\Filters;

use Illuminate\Http\Request;

abstract class Filters
{

    protected $request,$builder;
    protected $filters = [];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param $builder
     * @return mixed
     */
    public function apply($builder)
    {
        $this->builder = $builder;

        if(method_exists($this, $filter = $this->getFilter())) {
            $this->$filter();
        }

        return $this->builder;
    }

    public function getFilter()
    {
        if (in_array($filter = $this->request->query('filter'), $this->filters)) {
            return $filter;
        }

        return 'defaultFilter';
    }
}