<?php

namespace App\Filters;

use App\User;

class ThreadsFilters extends Filters
{
    protected $filters = ['by','popularity','unanswered','recent'];

    /**
     * @param $username
     * @return mixed
     */
    protected function by($username)
    {
        $user = User::where('name', $username)->firstOrfail();

        return $this->builder->where('user_id', $user->id);
    }

    public function popularity()
    {
        $this->builder->getQuery()->orders = [];
        return $this->builder->orderBy('replies_count','desc');
    }

    public function unanswered()
    {
        return $this->builder->where('replies_count',0);
    }

    public function recent()
    {
        return $this->builder;
    }

    public function defaultFilter()
    {
        return $this->popularity();
    }
}