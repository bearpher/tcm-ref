<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\View;
use Carbon\Carbon;
use App\Channel;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::setLocale('zh');

        \View::composer('*',function ($view){
            $channels = \Cache::rememberForever('channels',function (){
                return Channel::all();
            });
            $view->with('channels',$channels);
        });

        \Validator::extend('spamfree','App\Rules\SpamFree@passes');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
