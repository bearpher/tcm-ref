<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class ThreadHasNewReply
{
    use SerializesModels;
    public $thread;
    public $reply;

    /**
     * Create a new event instance. 已重构
     *
     * @param \App\Thread $thread
     * @param \App\Reply $reply
     */
    public function __construct($thread,$reply)
    {
        //
        $this->thread = $thread;
        $this->reply = $reply;
    }
}