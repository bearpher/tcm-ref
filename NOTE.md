
## 代码追踪

### 使用过的图标
thumb-up
### 前端权限重构步骤

封装authorization.js
signedIn注册为全局变量。
https://laravel-china.org/docs/forum-in-laravel-tdd/80-rights-reconfiguration/1951

只用owns方法，就可以判断当前用户是否是thread、reply的owner
https://laravel-china.org/docs/forum-in-laravel-tdd/83-best-reply-four/1960 

自动加载
当在composer.json中新加如文件后，运行

    composer dump-autoload

tinker
启动tinker后，可以在命令行和laravel进行交互

待优化

点赞次数在数据库层面限制去掉，程序中有限制就可以了
## 异常机制
第一种方法，直接在控制器中获取异常，然后返回错误码

第二种方法，在App\Exception\Handler中捕获异常

捕获spam错误流程 
在App\Request\CreatePostRequest中抛出ThrottleException 


## 创建事件
首先在EventServiceProvider创建事件和监听器数组，
然后 

    php artisan event:generate
  

## 经验
vue调试记录

以后使用yarn来安装，加载vue放到bootstrap.js文件

nav的$channel在缓存中，数据库refresh后要cache:clear

.vue页面修改后刷新没效果，有缓存问题时,执行下面两个命令试试

    npm run watch -- --watch-poll
或

    npm run dev
或
删除chrome缓存

重新迁移数据备注：

    php artisan migrate:refresh
    php artisan cache:clear
    php artisan db:seed

## 调试记录
axios请求post非法数据，后端返回422状态码时报错：
Uncaught (in promise) TypeError: Cannot read property 'data' of undefined

最后通过分析，
原因时当后端返回状态码422时，执行流程依然进入then(){}里面
参考 https://cn.vuejs.org/v2/cookbook/using-axios-to-consume-apis.html
axios的catch()方法应该放到then()后面，如这样
    
    axios.post(location.pathname + '/replies', { body: this.body })
        .then(({data}) => {
            this.body = '';
            flash('Your reply has been posted.');
            this.$emit('created', data);
        }).
        catch(error => {
            console.log('ERROR');
            console.log(error.response);
        });

还有一点需要注意，catch()中打印error格式为error.response
    
Request failed with status code

备注
安装redis时提示

    composer require predis/predis

predis/predis suggests installing ext-phpiredis 
(Allows faster serialization and deserialization of the Redis protocol)

## 项目名称更改记录
从commit 607aa5e 项目名称forum 改为tcm-ref(中医合参) 

