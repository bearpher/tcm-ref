@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                {{--<div class="page-header">--}}
                    {{--<h1>--}}
                        {{--{{ $profileUser->name }}--}}
                        {{--<small>注册于{{ $profileUser->created_at->diffForHumans() }}</small>--}}
                    {{--</h1>--}}
                {{--</div>--}}

                {{--@can('update',$profileUser)--}}
                    {{--<form method="POST" action="{{ route('avatar',$profileUser) }}" enctype="multipart/form-data">--}}
                        {{--{{ csrf_field() }}--}}

                        {{--<input type="file" name="avatar">--}}

                        {{--<button type="submit" class="btn btn-primary">Add Avatar</button>--}}
                    {{--</form>--}}
                {{--@endcan--}}

                {{--<img src="/storage/{{ $profileUser->avatar() }}" width="200" height="200">--}}

                <avatar-form :user="{{ $profileUser }}"></avatar-form>

                @forelse($activities as $date => $activity)
                    <h3 class="page-header">{{ $date }}</h3>

                    @foreach($activity as $record)
                        @if(view()->exists("profiles.activities.{$record->type}"))
                            @include("profiles.activities.{$record->type}",['activity'  => $record])
                        @endif
                    @endforeach
                @empty
                    <p>There is no activity for this user yet.</p>
                @endforelse

                <br>
                <br>
                发布的主题

                @foreach($threads as $thread)
                    <div class="card" style="margin-bottom: 1rem">
                        <div class="card-header">
                            <div class="level">
                            <span class="flex">
                            <a href="{{ route('profile',$thread->creator) }}">{{ $thread->creator->name }}</a> 发表于
                            <a href="{{ $thread->path() }}"> {{ $thread->title }} </a>
                            </span>

                            <span>{{ $thread->created_at->diffForHumans() }}</span>
                            </div>
                        </div>

                        <div class="card-body">
                            {{ $thread->body }}
                        </div>
                    </div>
                @endforeach

                {{ $threads->links() }}
            </div>
        </div>
    </div>
@endsection