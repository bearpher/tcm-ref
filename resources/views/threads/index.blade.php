@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-9 thread-list-container">
                <ul class="list-group">
                    <li class="list-group-item list-group-item-action topic-filter">
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a class="@if ($filter == 'popularity' || $filter == '') active @endif"
                                   href="/threads?filter=popularity">活跃</a></li>
                            <li class="list-inline-item">
                                <a class="@if ($filter == 'recent') active @endif"
                                   href="/threads?filter=recent">最近</a>
                            </li>
                            <li class="list-inline-item">
                                <a class="@if ($filter == 'unanswered') active @endif"
                                   href="/threads?filter=unanswered">零回复</a>
                            </li>
                        </ul>
                    </li>
                    @include ('threads._list')
                    @if ($threads->total() > 20)
                        <li class="list-group-item list-group-item-action pagination-height">
                            <div class="float-right">
                                {{ $threads->render() }}
                            </div>
                        </li>
                    @endif
                </ul>
            </div>

            <div class="col-lg-3">
                @if(count($trending))
                    <ul class="list-group">
                        <li class="list-group-item list-group-item-action">
                            热门话题
                        </li>
                        @foreach($trending as $thread)
                            <li class="list-group-item list-group-item-action reset-list-group-padding">
                                <div class="list-height">
                                    <a href="{{ url($thread->path) }}">
                                        {{ $thread->title }}
                                    </a>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
    </div>
@endsection