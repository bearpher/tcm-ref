{{-- Edit --}}
<div class="article" v-if="editing">

        <div class="form-group">
            <input type="text" class="form-control" v-model="form.title">
        </div>

        <div class="form-group">
            <textarea class="form-control" rows="10" v-model="form.content"></textarea>
        </div>
        <div class="form-group">
            <button class="btn btn-xs ml-2" @click="update">更新</button>
            <button class="btn btn-xs" @click="resetForm">取消</button>
            @can('update',$thread)
                <form action="{{ $thread->path() }}" method="POST" class="float-left">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

                    <button type="submit" class="btn btn-xs">删除</button>
                </form>
            @endcan
        </div>

</div>

{{-- View --}}
<div class="article" v-else>

        <div class="header">
            <h1 class="title">
                 <span v-text="title"></span>
            </h1>
            <div class="meta">
                创建于{{ $thread->created_at->diffForHumans() }} /

            </div>
        </div>

        <div class="divider"></div>

        <div class="body">
            <div v-text="content"></div>
        </div>

</div>