@extends('layouts.app')

@section('header')
    {{--<link rel="stylesheet" href="/css/vendor/jquery.atwho.css">--}}

@endsection

@section('content')
    <thread-view :thread="{{ $thread }}" inline-template>
        <div class="container">

            <div class="row">
                <div class="col-md-8">

                    @include('threads._topic')

                    <div class="component-1-button">
                        <button  class="btn btn-xs"v-if="authorize('owns',thread)" @click="editing = true">编辑</button>
                        <subscribe-button :active="{{ json_encode($thread->isSubscribedTo)}}"></subscribe-button>
                        <button class="btn btn-default" v-if="authorize('isAdmin')" @click="toggleLock" v-text="locked ? '解锁' : '锁定'"></button>
                    </div>

                    <div class="comments">
                        <div class="comment-divider">
                            <span class="icon comment-divider-text">讨论数量 <span v-text="repliesCount"></span></span>
                        </div>

                        <replies @added="repliesCount++" @removed="repliesCount--"></replies>
                    </div>


                </div>

                <div class="col-md-4">
                    <div class="card-user">
                        <div>
                            <a href="{{ route('profile',$thread->creator) }}">
                                <img class="avatar" src="{{ asset('/storage/'.$thread->creator->avatarPath) }}">
                            </a>
                        </div>
                        <div class="div-name">
                            <a class="name" href="{{ route('profile',$thread->creator) }}">
                                {{ $thread->creator->name }}
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </thread-view>
@endsection