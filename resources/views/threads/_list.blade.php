@forelse ($threads as $thread)
    <li class="list-group-item list-group-item-action reset-list-group-padding">
        <div class="item-topic-container">
            <div class="item-topic-left">
                <a href="{{ route('profile',$thread->creator) }}">
                    <img class="avatar-thread" src="{{ asset('storage/'.$thread->creator->avatarPath) }}" alt="{{ $thread->creator->name }}" />
                </a>
            </div>
            <div class="item-topic-right thread-nums">
                <span title="访问数">{{ $thread->visits() }}</span>
                <span class="thread-nums-other">
                    /
                    <span title="回复数">{{ $thread->replies_count }}</span>
                </span>
            </div>
            <div class="item-topic-center">
                <a href="{{ $thread->path() }}">
                    @if(auth()->check() && $thread->hasUpdatesFor(auth()->user()))
                        {{ $thread->title }}
                    @else
                        <span class="have-read">
                        {{ $thread->title }}
                        </span>
                    @endif
                </a>
            </div>
        </div>
    </li>


@empty
    <li class="list-group-item list-group-item-action">
         暂时没内容
    </li>
@endforelse