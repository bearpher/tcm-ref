# Larum

## 关于Larum

参考[Let's Build A Forum with Laravel and TDD ](https://laracasts.com/series/lets-build-a-forum-with-laravel) 构建的论坛系统

中文版教程 [TDD 构建 Laravel 论坛笔记](https://learnku.com/docs/forum-in-laravel-tdd) 

## 与原教程的不同

- 基于Laravel5.6开发，页面样式使用sass,bootstrap4版本，响应式设计
- 修改了主题路由query的格式为?filter=xxx，相对美观
- 增加了简体，繁体，英文支持

## 感谢

感谢作者,若喜欢该系列视频，可去该网站订阅后下载该系列视频， 支持正版