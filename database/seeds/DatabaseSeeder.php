<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         factory('App\Channel',4)->create();
         factory('App\Reply',40)->create([
             'thread_id'=> function() {
                 return factory('App\Thread')->create([
                     'channel_id'=> rand(1, 4)
                 ])->id;
             }
         ]);
         App\User::first()->notify(new App\Notifications\ThreadWasUpdated(App\Thread::first(),App\Reply::first()));
    }
}
