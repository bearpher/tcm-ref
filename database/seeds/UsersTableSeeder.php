<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'bbbb',
            'email' => 'bbbb@qq.com',
            'password' => bcrypt('123456'),
            'confirmed' => true,
            'created_at'=> "2018-09-06 04:56:34",
            'updated_at'=> "2018-09-06 04:56:34",
        ]);

        DB::table('users')->insert([
            'name' => 'cccc',
            'email' => 'cccc@qq.com',
            'confirmed' => true,
            'password' => bcrypt('123456'),
            'created_at'=> "2018-09-07 04:56:34",
            'updated_at'=> "2018-09-07 04:56:34",
        ]);
    }
}
